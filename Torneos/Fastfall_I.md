﻿¡Te damos la bienvenida al primer torneo de **Super Smash Bros. Ultimate** organizado por **MAD Smash**, la comunidad madrileña de **SmashBrosSpain**!

Se realizará el día 29 de diciembre de 2018 en **UNTTS Gaming Lounge**, cerca de Embajadores. Consulta el mapa más abajo.

## Fastfall
Con un nuevo juego comienza un nuevo viaje hacia la victoria... ¿llegarás al final o caerás por el camino?

El torneo contará con **sorteos**, **invitados especiales**, retransmisión en el **canal oficial** de SmashBrosSpain y, por supuesto, ¡**un montón de gente** con la que divertirte y aprender! ¿Te lo vas a perder?

## Inscripción
El precio de la inscripción se compone:
- 3€ de ***venue fee*** (gastos de local y organización)
- 3€ del **torneo individual**, que irán íntegramente al bote del premio
- Para el **torneo por parejas**, cada pareja aportará 2€, que irán íntegramente al bote del premio de dicho torneo

## Normas
- Las *setups* (consola + televisión) tienen prioridad para el torneo. Pero si no hay torneo, ¡**siéntete libre** de utilizarlas!
- A todos nos encanta jugar, ¡pero no te acomodes! **Rota** con los demás asistentes para que juegue todo el mundo.
- Debemos cuidar y respetar el local en el que se realiza el evento. Por ello está **prohibido** consumir alimentos y bebidas no adquiridas en el local.
- Con el fin de disfrutar de un **ambiente agradable**, rogamos el máximo respeto entre los asistentes al evento. En caso de disrupción del mismo los organizadores tomarán medidas con el fin de mitigarla.
- Lee el resto de normas más abajo.
