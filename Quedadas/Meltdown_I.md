¡Te damos la bienvenida a la **II Quedada Meltdown**! Un nuevo evento de **Super Smash Bros. Ultimate** organizado por **MAD Smash**, la comunidad madrileña de **SmashBrosSpain**.

Se realizará el 23 de diciembre de 2018 a las 12 de la mañana en **Meltdown Madrid**, cerca de las paradas de Metro Iglesia y Bilbao. Consulta el mapa más abajo.


## *Ladder*
Queremos que sea un evento que facilite a quienes no han jugado competitivamente iniciarse en este mundillo. Por ello el formato será una *ladder*, que permitirá que todo el mundo juegue un número similar de partidas, además de equilibrar el nivel de las mismas.

Si ya juegas competitivo, ¡sé amable y enseña lo que sabes!

## Inscripción
La inscripción será gratuita, puesto que no habrá ningún premio. Sin embargo, será **obligatorio** al menos una consumición por asistente.


## Normas
- A todos nos encanta jugar, ¡pero no te acomodes! **Rota** con los demás asistentes para que juegue todo el mundo.
- Debemos cuidar y respetar el local en el que se realiza el evento. Por ello está **prohibido** consumir alimentos y bebidas no adquiridas en el local.
- Con el fin de disfrutar de un **ambiente agradable**, rogamos el máximo respeto entre los asistentes al evento. En caso de disrupción del mismo los organizadores tomarán medidas con el fin de mitigarla.
- Lee el resto de normas más abajo.