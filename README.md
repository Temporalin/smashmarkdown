# SmashMarkdown

Archivos para mantener las reglas actualizadas y en formato adecuado para Smash.gg

# ¿Cómo utilizo los archivos?
El formato Markdown, utilizado por Smash.gg, tiene como extensión de archivo .md

En el repositorio encontrarás distintos archivos con este formato. Busca el que necesitas, ábrelo y, en los botones que aparecen en la esquina superior derecha, pulsa en el que tiene el icono **</>** (*Display source*) o en el que tiene ese icono dentro de un papel (*Open raw*)

# Enlaces rápidos
[Reglas](https://gitlab.com/Temporalin/smashmarkdown/raw/master/Reglas.md)

[Descripción del torneo FastFall I](https://gitlab.com/Temporalin/smashmarkdown/raw/master/Torneos/Fastfall_I.md)

[Descripción de la quedada Meltdown I](https://gitlab.com/Temporalin/smashmarkdown/raw/master/Quedadas/Meltdown_I.md)

