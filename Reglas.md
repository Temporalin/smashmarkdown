Reglas actualizadas a la normativa de SmashBrosSpain.

## Normas del evento
- Trae tu propio mando. Si no tienes quizás puedas usar uno prestado, pero no cuentes con ello.
- Mandos 100% permitidos: GameCube, Pro de Nintendo Switch, JoyCon.
- Mandos permitidos pero no aconsejados: todos aquellos licenciados por Nintendo, como los de Hori o los de PDP. Eso sí, si en algún momento provocan problemas durante una partida **se considerará descalificación automática**.
- Si es un mando inalámbrico, desincronízalo de las consolas en las que juegues.
- Del mismo modo, trae tu propia Switch si es posible. Así evitaremos problemas a la hora de jugar con los demás.
- En caso de que una persona quiera jugar con auriculares y otra no, tendrá **prioridad** quien use auriculares **sólo si lleva _splitter_**.
- La organización procurará proteger el material de los asistentes pero **no garantizamos su seguridad**. Por favor, mantén tus cosas vigiladas.

## Normas del torneo
- Torneo de eliminación doble: se debe perder dos veces para ser eliminado.
- El ganador del set debe reportar su victoria a la organización.
- Los primeros sets serán al **mejor de 3** y según avancen las rondas pasarán a ser al **mejor de 5**, dependiendo del número de asistentes.
- La organización tiene el derecho a subir a internet cualquier partida. Negarse a jugar en *stream* puede resultar en descalificación.
- Está prohibido pactar resultados. Si la organización sospecha que ha habido un pacto, todos los jugadores envueltos en éste podrían ser descalificados
- Dar consejos en mitad de un set a otros jugadores NO está permitido. Esto incluye contactar vía teléfono. Se pueden consultar notas propias. No seguir esta norma puede resultar en una descalificación.
- Si un jugador no aparece a los 10 minutos de ser llamado a jugar, éste perderá la primera partida. Si llega 15 minutos tarde, perderá el set entero.

## Normas de juego
- 3 vidas, 8 minutos.
- Objetos desactivados, frecuencia nula.
- *Hazards* (mecanismos) desactivados.
- La pausa estará desactivada. Para interrumpir una partida se llamará a la organización únicamente si un personaje está en la plataforma de reaparición. Si la razón no es justificable, quien haya interrumpido la partida deberá quitarse una vida (*stock*). En caso de que la opción de pausa estuviera activada, si se ha usado sin un personaje en la plataforma de reaparición, quien la haya activado deberá quitarse automáticamente una vida.
- Los Miis están permitidos con cualquier movimiento.
- El resultado del combate será el que dicte la pantalla de resultados.
- Si se acaba el tiempo ganará el jugador con más vidas, o el que tuviera menos %.
- Si hubiera empate, se jugará una partida de 1 vida, 3 minutos, en el mismo escenario y con los mismos personajes.
- El escenario se elegirá por *stage striking* (más información en *Procedimiento de un set*).
- Cada jugador puede *banear* hasta 3 escenarios, los cuales no podrán ser escogidos por el rival cuando le toque hacer *counterpick*. Estos *bans* se pueden modificar entre partidas.
- Norma de Fei: cada jugador tiene un minuto entre partidas para mirar sus notas y hacer *counterpick*. Si se tarda más, el oponente podrá empezar la partida directamente en la pantalla legal que quiera.
- Se puede calentar antes del set y así comprobar los controles. No ha de durar más de 30 segundos.
- Si una consola, mando o TV da problemas e interfiere en el desarrollo de un set, los jugadores deberán contactar con la organización y seguir sus indicaciones.

## Normas adicionales para dobles
- Fuego amigo activado
- Permitido compartir vidas
- Si se acaba el tiempo con el mismo número de vidas en ambos equipos, el ganador será el equipo con un % total más bajo
- Los jugadores deben, en la medida de lo posible, usar trajes del color de su equipo. Por ejemplo, el color azul de Peach si son del equipo azul
- Si un equipo lleva 2 luchadores Mii del mismo tipo, estos deben estar diferenciados con un sombrero
- Está prohibido intercambiar mandos entre jugadores
- Se usará piedra-papel-tijeras para decidir el color de los equipos si no hay un acuerdo

## Procedimiento de un set
1. Se juega un piedra-papel-tijeras para decidir quién empieza
2. El ganador *banea* (elimina) un escenario de las posibles, el oponente elimina otros dos y el ganador decide finalmente en qué escenario se jugará la primera partida
3. Los jugadores escogen sus personajes. Si los jugadores no se ponen de acuerdo en la elección de personajes, se elegirán a ciegas. Ambos jugadores dirán su personaje a una persona ajena a la partida o lo escribirán en su móvil, enseñando así su personaje a la vez.
4. Se juega la primera partida.
5. Los escenarios adicionales (*counterpick*) se añaden a los escenarios seleccionables. Tras ello, el ganador *banea* los escenarios correspondientes.
6. El perdedor elige el siguiente escenario.
7. El ganador elige su personaje.
8. El perdedor elige su personaje, sin que el ganador pueda cambiar después.
9. Se repiten los pasos del 4 al 8 hasta que se complete el set.


## Escenarios

### Iniciales / *starters*:
- Campo de Batalla / *Battlefield*
- Destino Final / *Final Destination*
- Estadio Pokémon 2 / *Pokemon Stadium 2*
- Pueblo Smash / *Smashville*
- Sobrevolando el pueblo / *Town and City*

### Adicionales / *counterpicks*:
- Sistema Lylat / *Lylat Cruise*
- Liga Pokémon de Kalos / *Kalos Pokemon League*
- Liga Pokémon de Teselia / *Unova Pokemon League*
- Isla de Yoshi: Yoshi's Story / *Yoshi's Island: Yoshi's Story*

## Premios
Lo recaudado de las inscripciones se repartirá del siguiente modo:

### <9 Jugadores:
- 1º 70 %
- 2º 30 %

### 9-32 Jugadores:
- 1º 60 %
- 2º 30 %
- 3º 10 %

### 33-64 Jugadores:
- 1º 50 %
- 2º 25 %
- 3º 15 %
- 4º 10 %
